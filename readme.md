## multikey-map

A `Map`-like structure with array keys

[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/seangenabe/multikey-map.svg?style=flat-square)](https://gitlab.com/seangenabe/multikey-map)
[![npm](https://img.shields.io/npm/v/@seangenabe/multikey-map.svg?style=flat-square)](https://www.npmjs.com/package/@seangenabe/multikey-map)

## Usage

```typescript
import { MultikeyMap } from "@seangenabe/multikey-map"

const map = new MultikeyMap<[number, number, number], string>()

map.set([1, 2, 3], "a")
map.get([1, 2, 3]) // "a"
```

* All `Map` methods are implemented except that the keys are arrays.
* Empty array keys (`[]`) are supported.
* Storing keys differing in lengths are supported.
* Keys must always be an array.
* Insertion order is not kept.

### Map implementation

* `get size(): number`
* `get(key: K): V | undefined`
* `set(key: K, value: V): this`
* `delete(key: K): boolean`
* `has(key: K): boolean`
* `clear(): void`
* `entries(): IterableIterator<[K, V>]`
* `[Symbol.iterator](): IterableIterator<[K, V]>`
* `keys(): IterableIterator<K>`
* `values(): IterableIterator<V>`
* `forEach(callbackFn: (value: V, key: K, map: this) => void, thisArg?: any): void`
* `get [Symbol.toStringTag](): string`

### `map.setKV(key, value)`

```
setKV(key: K, value: V): boolean
```

Same as `set` except it returns a boolean indicating whether the value has been inserted. Otherwise, the value has replaced an existing value.

### `map.prefixEntries(prefixKey)`

```
prefixEntries(prefixKey: SubArray<K>): IterableIterator<[K, V]>
```

Returns all entries whose key starts with the given prefix.
